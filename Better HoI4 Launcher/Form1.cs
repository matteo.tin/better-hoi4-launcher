﻿using Better_HoI4_Launcher.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Better_HoI4_Launcher
{
	public partial class Form1 : Form
	{
		SavegamesReader savegamesReader = new SavegamesReader();
		ModsReader modsReader = null;
		Point lastPoint;

		private bool isLaunchingBySavegame = true;

		public Form1()
		{
			InitializeComponent();

			try
			{
				modsReader = new ModsReader();

				foreach (Tuple<int,string> mod in modsReader.mods.OrderBy(c => c.Item2))
					this.checkedListBox1.Items.Add(mod.Item2);
			}
			catch (InvalidOperationException ex)
			{
				MessageBox.Show(ex.Message);
			}

			foreach (string savegame in savegamesReader.savegames)
				listBox1.Items.Add(savegame);

			this.panel2.Visible = false;

		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			string selectedItem = ((dynamic)sender).SelectedItem;
			this.label2.Text = $"Savegame: {selectedItem}";

			listBox2.Items.Clear();
			try
			{
				List<string> mods = savegamesReader.RetrieveMods(selectedItem);

				foreach (string mod in mods)
					if(!String.IsNullOrEmpty(mod))
						listBox2.Items.Add(mod);
			}
			catch (NotSupportedException ex)
			{
				MessageBox.Show($"{ex.Message}\r\nTo save to this format, please modify setting.txt the string save_as_binary=no.\r\nThe file is located usually in your documents folder");
			}

			checkForSavegames();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			List<int> modsToLaunch = new List<int>();
			if(isLaunchingBySavegame)
			{
				foreach (object o in listBox2.Items)
				{
					string modName = o.ToString();

					Tuple<int, string> mod = modsReader.mods.FirstOrDefault(c => c.Item2.ToLower() == modName.ToLower());
					if (mod == null)
						throw new KeyNotFoundException($"mod \"{modName}\" was not found in your workshop folder");

					modsToLaunch.Add(mod.Item1);
				}
			}
			else
			{
				foreach (object o in checkedListBox1.CheckedItems)
				{
					string modName = (string)o;

					Tuple<int, string> mod = modsReader.mods.FirstOrDefault(c => c.Item2.ToLower() == modName.ToLower());
					if (mod == null)
						throw new KeyNotFoundException($"mod \"{modName}\" was not found in your workshop folder");

					modsToLaunch.Add(mod.Item1);
				}
			}

			string modString = null;
			foreach (int id in modsToLaunch)
				modString = $"{modString} -mod=mod/ugc_{id}.mod";

			string gamePath = $"\"F:\\Games\\Steam\\steamapps\\common\\Hearts of Iron IV\\hoi4.exe\"";
			string launchCommand = $" -nolauncher{modString}";

			Process.Start(gamePath, launchCommand);

			//MessageBox.Show(launchCommand);

			this.Close();

		}

		private void button4_Click(object sender, EventArgs e)
		{
			checkForMods();
			this.panel2.Visible = true;
			this.panel1.Visible = false;
			this.panel2.BringToFront();
			this.button4.BringToFront();
			this.isLaunchingBySavegame = false;

		}

		private void button2_Click(object sender, EventArgs e)
		{
			checkForSavegames();
			this.panel1.Visible = true;
			this.panel2.Visible = false;
			this.panel1.BringToFront();
			this.button2.BringToFront();
			this.isLaunchingBySavegame = true;

		}

		private void checkedListBox1_CheckItem(object sender, EventArgs e)
		{
			checkForMods();
		}

		private void checkForSavegames()
		{
			this.button1.Enabled = listBox2.Items.Count > 0;
			this.button1.BackgroundImage = this.button1.Enabled ? Properties.Resources.play : Properties.Resources.no_play;
		}

		private void checkForMods()
		{
			bool hasModsSelected = this.checkedListBox1.CheckedItems.Count > 0;

			this.button1.Enabled = hasModsSelected;
			this.button1.BackgroundImage = this.button1.Enabled ? Properties.Resources.play : Properties.Resources.no_play;
		}

		private void panel3_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				this.Left += e.X - lastPoint.X;
				this.Top += e.Y - lastPoint.Y;
			}
		}

		private void panel3_MouseDown(object sender, MouseEventArgs e)
		{
			lastPoint = new Point(e.X, e.Y);
		}

		private void checkedListBox1_CheckItem(object sender, ItemCheckEventArgs e)
		{

		}

		private void button5_Click(object sender, EventArgs e)
		{
			Options o = new Options();
			o.Show();
		}
	}
}
