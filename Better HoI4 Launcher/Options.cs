﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Better_HoI4_Launcher
{
	public partial class Options : Form
	{
		public Options()
		{
			InitializeComponent();
			this.textBox1.Text = Properties.Settings.Default.Gamepath;
			this.textBox2.Text = Properties.Settings.Default.WorkshopPath;
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			Properties.Settings.Default.Gamepath = this.textBox1.Text;
			Properties.Settings.Default.Save();
		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{
			Properties.Settings.Default.WorkshopPath = this.textBox2.Text;
			Properties.Settings.Default.Save();
		}

		private void button3_Click(object sender, EventArgs e)
		{
			this.Close();
		}

	}
}
