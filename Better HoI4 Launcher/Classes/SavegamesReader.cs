﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Better_HoI4_Launcher.Classes
{
	class SavegamesReader
	{
		private string documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
		private string internalPath = "Paradox Interactive\\Hearts of Iron IV\\save games";
		public List<string> savegames = new List<string>();

		public SavegamesReader()
		{
			DirectoryInfo info = new DirectoryInfo($"{documents}\\{internalPath}");
			FileInfo[] files = info.GetFiles().OrderByDescending(p => p.LastWriteTime).ToArray();
			foreach (FileInfo file in files)
			{
				savegames.Add(file.Name);
			}	

		}

		public List<string> RetrieveMods(string fileName)
		{
			using (StreamReader file = File.OpenText($"{documents}\\{internalPath}\\{fileName}"))
			{
				string fileString = file.ReadToEnd();
				if (fileString.Contains("HOI4bin"))
					throw new NotSupportedException("This savegame is in binary version. Please, resave it in string version");

				else
				{
					fileString = fileString.Substring(fileString.IndexOf("mods=") + 5, fileString.Length - fileString.IndexOf("mods=") - 5);
					fileString = fileString.Substring(0, fileString.IndexOf("}") + 1);
					fileString = fileString.Replace("\r\n\t", ",");
					fileString = fileString.Replace("{", "[").Replace("}", "]");

					JToken o2 = JsonConvert.DeserializeObject<JToken>(fileString);

					return o2.ToObject<List<string>>();
				}
			}
		}
	}
}
