﻿using Ionic.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Better_HoI4_Launcher.Classes
{
	class ModsReader
	{
		private string path = Properties.Settings.Default.Gamepath; //@"F:\Games\Steam\steamapps\common\Hearts of Iron IV";
		private string workshopPath = Properties.Settings.Default.WorkshopPath;//@"F:\Games\Steam\steamapps\workshop\content";
		public string appId = null;

		public List<Tuple<int, string>> mods = new List<Tuple<int, string>>();

		public ModsReader()
		{
			if (String.IsNullOrEmpty(path) || String.IsNullOrEmpty(workshopPath))
				throw new InvalidOperationException("Set the correct paths in the options and reboot the launcher!");

			using (StreamReader file = new StreamReader($@"{path}\steam_appid.txt"))
				appId = file.ReadToEnd();

			workshopPath = $@"{workshopPath}\{appId}";
			ReadMods();
		}

		private void ReadMods()
		{
			foreach (string directoryPath in Directory.GetDirectories(workshopPath))
			{
				string zipFile = Directory.GetFiles(directoryPath)?.FirstOrDefault(c => c.ToLower().EndsWith(".zip"));
				if(zipFile != null)
				{
					using (ZipFile zip = ZipFile.Read(zipFile))
					{
						ZipEntry e = zip["descriptor.mod"];
						using (MemoryStream ms = new MemoryStream())
						{
							e.Extract(ms);

							ms.Position = 0;
							using(StreamReader sr = new StreamReader(ms))
							{
								string descriptor = sr.ReadToEnd();

								string modNameKey = "name=";
								string modIdKey = "remote_file_id=";

								string modId = descriptor.Substring(descriptor.IndexOf(modIdKey) + 
									modIdKey.Length, descriptor.Length - descriptor.IndexOf(modIdKey) - modIdKey.Length);
								modId = modId.Substring(modId.IndexOf("\"") + 1);
								modId = modId.Substring(0, modId.IndexOf("\""));

								if (!Int32.TryParse(modId, out int modIdInt))
									modIdInt = -1;

								string modName = descriptor.Substring(descriptor.IndexOf(modNameKey) + 
									modNameKey.Length, descriptor.Length - descriptor.IndexOf(descriptor) - modNameKey.Length);

								modName = modName.Substring(modName.IndexOf("\"") + 1);
								modName = modName.Substring(0, modName.IndexOf("\""));

								mods.Add(new Tuple<int, string>(modIdInt, modName));
							}
						}
					}
				}
			}
		}

	}
}
