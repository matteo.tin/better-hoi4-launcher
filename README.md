# Better Hoi4 Launcher
It's not a better launcher, it's simply a better Hoi4 mod launcher!

## WTH is this?
This mod launcher is capable of read your saved games and activate the correct mods for that specific saved game.

You don't need to remember furthermore what mods you used in a specific multiplayer game, because BHOI4L do it for you! :)

## Awesome! How this works!?
It reads the mods you used inside savegame file if it is saved as Text format

## Text format, Uh?
Hearts of Iron use a specific cryptographic method to save their savegames to avoid manipulation/performance/size issues.

However it's possibile to save your games in a text format which is easilly readable from any text editor, or in this case, software.

You can change your configuration following
[this thread](https://forum.paradoxplaza.com/forum/index.php?threads/how-to-edit-hoi4-savegame.943682/).

## I have already a savegame, can i convert it?
Yes. Follow these steps:
1. Change the configuration
2. Open the game and load your savegame
3. Save the game
4. ....
5. Profit!

## Great. But my savegame is a multiplayer one..
No problem. Follow the above steps and load your savegame in a singleplayer session.

You and your fellows will no lose any progress when load it as multiplayer match.

## I'm goin' to download it! I need to configure something?
Great to hear!

Yes, you need to configure two little things in the options (bottom-left icon):
1. The game path, or where the game is located. The path must include the ".exe" file. Usually the game is installed in: `C:\Program Files (x86)\Steam\steamapps\common\Hearts Of Iron 4\hoi4.exe"`
2. The mods path, where the steam mods are stored. It's usually in: `\Documents\Paradox Interactive\Hearts of Iron IV\mod`. The folder must contains only `*.mod` files.
3. Reboot the launcher (it's a known bug!)

If you did every steps correct, everything should works fine. If you encounter any problem, feel free to open an issue!